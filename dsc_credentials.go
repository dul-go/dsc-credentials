package dsc_credentials

import (
	"fmt"
	"os"

	"gopkg.in/ini.v1"
)

// DSC_Credentials represents the access key id and secret access key data used to authenticate against the Data Service Center (hosted in AWS)
type DSC_Credentials struct {
	AccessKeyId     string
	SecretAccessKey string
	SessionId       string
}

// NewCredentials will attempt to read the credentials from an INI file (credentialsFile)
// with the format of:
//
// [default]
// aws_access_key_id=
// aws_secret_access_key=
//
// If the file does not exists, or some other read-error occurs,
// then search for the needed data in environment variables:
// - AWS_ACCESS_KEY_ID
// - AWS_SECRET_ACCESS_KEY
//
// Returns reference to a DSC_Credentials struct or an error
func NewCredentials(credentialsFile string) (*DSC_Credentials, error) {
	readFromEnv := false
	if len(credentialsFile) > 0 {
		cfg, err := ini.Load(credentialsFile)
		if err != nil {
			fmt.Printf("Failed to read credentials file")
			readFromEnv = true
		} else {
			dsc := &DSC_Credentials{}
			dsc.AccessKeyId = cfg.Section("default").Key("aws_access_key_id").String()
			dsc.SecretAccessKey = cfg.Section("default").Key("aws_secret_access_key").String()
			// fmt.Printf("%+v\n", dsc)
			if len(dsc.AccessKeyId) == 0 {
				fmt.Println("**** [Access Key ID not found] ****")
			}
			if len(dsc.SecretAccessKey) == 0 {
				fmt.Println("**** [Secret Access Key not found] ****")
			}
			return dsc, nil
		}
	}
	if readFromEnv {
		// scan environment...
		accessKeyId, aKeyExists := os.LookupEnv("AWS_ACCESS_KEY_ID")
		secretAccessKey, sKeyExists := os.LookupEnv("AWS_SECRET_ACCESS_KEY")
		if aKeyExists && sKeyExists {
			return &DSC_Credentials{
				AccessKeyId:     accessKeyId,
				SecretAccessKey: secretAccessKey,
			}, nil
		}
	}
	return nil, fmt.Errorf("Unable to read DSC credentials")
}
