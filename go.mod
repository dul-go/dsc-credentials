module gitlab.oit.duke.edu/dul-go/dsc-credentials

go 1.21.3

require gopkg.in/ini.v1 v1.67.0

require github.com/stretchr/testify v1.9.0 // indirect
